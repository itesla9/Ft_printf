/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   xxx.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 16:19:29 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/04 14:52:53 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int			isflag(t_struct *ps)
{
	if (*(ps->ptr) == 32 || *(ps->ptr) == 43 || *(ps->ptr) == 45 ||
		*(ps->ptr) == 35 || *(ps->ptr) == 48 || *(ps->ptr) == 46 ||
		*(ps->ptr) == 42)
		return (1);
	return (0);
}

int			ismodifier(t_struct *ps)
{
	if (*(ps->ptr) == 'h' || *(ps->ptr) == 'l' || *(ps->ptr) == 'j' ||
		*(ps->ptr) == 'z')
		return (1);
	return (0);
}

int			isnum(t_struct *ps)
{
	if (*(ps->ptr) >= 48 && *(ps->ptr) <= 57)
		return (1);
	return (0);
}

int			ggg(t_struct *ps)
{
	static int	h;
	int			q;

	q = 0;
	if (*(ps->ptr) == 'j')
		q = 6;
	if (*(ps->ptr) == 'l' && *(ps->ptr + 1) == 'l')
		q = 5;
	if (*(ps->ptr) == 'l' && *(ps->ptr + 1) != 'l')
		q = 4;
	if (*(ps->ptr) == 'z')
		q = 3;
	if (*(ps->ptr) == 'h')
		h++;
	if (h % 2 == 0 && *(ps->ptr + 1) != 'h' && *(ps->ptr) == 'h')
		q = 1;
	if (h % 2 != 0 && *(ps->ptr + 1) != 'h' && *(ps->ptr) == 'h')
		q = 2;
	return (q);
}

t_struct	*ft_cheak_struct(t_struct *ps)
{
	if (ps->hyphen == 1)
		ps->zero = 0;
	if (ps->plus == 1)
		ps->space = 0;
	return (ps);
}
