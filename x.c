/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   x.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:51:31 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/03 20:31:39 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void			ft_d2(t_struct *ps, va_list ap)
{
	long long int	xxx;
	int				len;

	if (*(ps->ptr) == 'D')
		xxx = va_arg(ap, long);
	if ((*(ps->ptr) == 'd' || *(ps->ptr) == 'i') && ps->a != 0)
	{
		if (ps->a >= 3 && ps->a <= 6)
			xxx = va_arg(ap, long long);
		if (ps->a == 2)
			xxx = (short int)va_arg(ap, long);
		if (ps->a == 1)
			xxx = (signed char)va_arg(ap, int);
	}
	len = ft_len(xxx);
	if (ps->width <= ps->precision)
		ft_width_less_precision(ps, xxx, len);
	else
		ft_width_bigger_precision(ps, xxx, len);
}

void			ft_p(t_struct *ps, va_list ap)
{
	long long	xxx;
	int			len;
	char		*fresh;

	len = 0;
	xxx = va_arg(ap, long long);
	fresh = ft_itoa_base(xxx, 16, len);
	fresh = ft_magic(fresh);
	len = ft_strlen2(fresh);
	if (ps->width <= ps->precision)
		ft_width_less_precision_p(ps, fresh, len);
	else
		ft_width_bigger_precision_p(ps, fresh, len);
	free(fresh);
}

void			ft_width_bigger_precision_x(t_struct *ps, char *fresh, int len)
{
	int	reserve_int;
	int	reserve;

	if (fresh[0] == '0')
		len = 0;
	reserve = ft_reserve_full(ps, len);
	if (ps->precision > len)
		reserve_int = ps->precision - len;
	else
		reserve_int = 0;
	if (ps->shark == 1)
		reserve = reserve - 2;
	if (ps->precision != -1)
	{
		if (ps->precision > len)
			ft_hyphen1_0_x(ps, fresh, reserve, reserve_int);
		if (ps->precision <= len && ps->precision >= 0)
			ft_hyphen2_0_x(ps, fresh, reserve);
	}
	if (ps->precision == -1)
		ft_hyphen3_0_x(ps, fresh, reserve);
}

void			ft_width_less_precision_x(t_struct *ps, char *fresh, int len)
{
	int	repeat;

	if (fresh[0] == '0')
		return ;
	if (ps->shark == 1 && *(ps->ptr) == 'x')
		ft_putstr("0x");
	if (ps->shark == 1 && *(ps->ptr) == 'X')
		ft_putstr("0X");
	if (ps->precision > len)
	{
		repeat = ps->precision - len;
		while (repeat > 0)
		{
			ft_putchar('0');
			repeat--;
		}
		ft_putstr(fresh);
	}
	else
		ft_putstr(fresh);
}
