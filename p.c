/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   p.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/30 19:03:20 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/01 12:25:39 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	ft_putspace_p(int reserve)
{
	while (reserve > 0)
	{
		ft_putchar(' ');
		reserve--;
	}
}

void	ft_hyphen1_0_p(t_struct *ps, char *fresh, int reserve, int reserve_int)
{
	if (ps->hyphen == 1)
	{
		ft_putstr("0x");
		while (reserve_int > 0)
		{
			ft_putchar('0');
			reserve_int--;
		}
		ft_putstr(fresh);
		ft_putspace(reserve, ps);
	}
	if (ps->hyphen == 0)
	{
		ft_putspace(reserve, ps);
		ft_putstr("0x");
		while (reserve_int > 0)
		{
			ft_putchar('0');
			reserve_int--;
		}
		ft_putstr(fresh);
	}
}

void	ft_hyphen2_0_p(t_struct *ps, char *fresh, int reserve)
{
	if (ps->hyphen == 1)
	{
		ft_putstr("0x");
		ft_putstr(fresh);
		ft_putspace(reserve, ps);
	}
	if (ps->hyphen == 0)
	{
		ft_putspace(reserve, ps);
		ft_putstr("0x");
		ft_putstr(fresh);
	}
}

void	ft_width_bigger_precision_p(t_struct *ps, char *fresh, int len)
{
	int	reserve_int;
	int reserve;

	if (ps->precision > len)
		reserve_int = ps->precision - len;
	else
		reserve_int = 0;
	reserve = ft_reserve_full(ps, len);
	reserve = reserve - 2;
	if (ps->precision != -1)
	{
		if (ps->precision > len)
			ft_hyphen1_0_p(ps, fresh, reserve, reserve_int);
		if (ps->precision <= len && ps->precision >= 0)
			ft_hyphen2_0_p(ps, fresh, reserve);
	}
	if (ps->precision == -1)
		ft_hyphen2_0_p(ps, fresh, reserve);
}

void	ft_width_less_precision_p(t_struct *ps, char *fresh, int len)
{
	int	repeat;

	if (ps->precision > len)
	{
		ft_putstr("0x");
		repeat = ps->precision - len;
		while (repeat > 0)
		{
			ft_putchar('0');
			repeat--;
		}
		ft_putstr(fresh);
	}
	else
	{
		if (fresh[0] == '0')
		{
			ft_putstr("0x");
			return ;
		}
		ft_putstr(fresh);
	}
}
