/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 12:42:02 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/04 14:21:40 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_H
# define HEADER_H
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <stdarg.h>
# include <string.h>

int g_stars;
typedef struct			s_struct
{
	int		width;
	int		precision;
	int		hyphen;
	int		plus;
	int		space;
	int		shark;
	int		zero;
	int		a;
	int		flag;
	char	*ptr;
}						t_struct;
unsigned long long		ft_put_sign(t_struct *ps, long long xxx);
void					ft_flags(t_struct *ps);
void					ft_fill_struct(t_struct *ps, va_list ap);
void					ft_s(t_struct *ps, va_list ap);
void					ft_c(t_struct *ps, va_list ap);
void					ft_final_c(char *zzz, t_struct *ps);
void					ft_putspace(int reserve, t_struct *ps);
int						ft_reserve(t_struct *ps, long long xxx, int len);
void					ft_hyphen1_0(t_struct *ps, long long xxx, int reserve,
									int reserve_int);
void					ft_hyphen2_0(t_struct *ps, long long xxx, int reserve);
void					ft_hyphen3_0(t_struct *ps, long long xxx, int reserve,
									unsigned long long yyy);
int						ft_printf(const char *restrict format, ...);
int						isflag(t_struct *ps);
int						ismodifier(t_struct *ps);
int						isnum(t_struct *ps);
int						ggg(t_struct *ps);
t_struct				*ft_cheak_struct(t_struct *ps);
int						ft_len(long long xxx);
void					ft_putnbr2(unsigned long long n, t_struct *ps);
void					ft_width_bigger_precision(t_struct *ps, long long xxx,
												int len);
void					ft_width_less_precision(t_struct *ps, long long xxx,
												int len);
void					ft_hyphen1_0_unsigned(t_struct *ps,
						unsigned long long xxx, int reserve, int reserve_int);
void					ft_hyphen2_0_unsigned(t_struct *ps,
						unsigned long long xxx, int reserve);
void					ft_hyphen3_0_unsigned(t_struct *ps,
						unsigned long long xxx, int reserve);
void					ft_widthbig_und(t_struct *ps, unsigned long long xxx,
										int len);
void					ft_widthles_und(t_struct *ps, unsigned long long xxx,
										int len);
int						ft_len_unsigned(unsigned long long xxx);
void					ft_d_unsigned(t_struct *ps, va_list ap);
void					ft_adjust(t_struct *ps, va_list ap);
char					*ft_itoa_base(long long xxx, int base, int len);
void					ft_width_bigger_precision_x(t_struct *ps,
											char *fresh, int len);
void					ft_width_less_precision_x(t_struct *ps, char *fresh,
												int len);
void					ft_hyphen1_0_x(t_struct *ps, char *fresh,
										int reserve, int reserve_int);
void					ft_hyphen2_0_x(t_struct *ps, char *fresh, int reserve);
void					ft_hyphen3_0_x(t_struct *ps, char *fresh, int reserve);
void					ft_width_bigger_precision_o(t_struct *ps,
													char *fresh, int len);
void					ft_width_less_precision_o(t_struct *ps, char *fresh,
													int len);
void					ft_hyphen1_0_o(t_struct *ps, char *fresh,
										int reserve, int reserve_int);
void					ft_hyphen2_0_o(t_struct *ps, char *fresh, int reserve);
void					ft_hyphen3_0_o(t_struct *ps, char *fresh, int reserve);
void					ft_hyphen1_0_p(t_struct *ps, char *fresh,
										int reserve, int reserve_int);
void					ft_hyphen2_0_p(t_struct *ps, char *fresh, int reserve);
int						ft_atoi(const char *str);
int						ft_strlen2(const char *s);
void					ft_putchar(char c);
void					ft_putstr(char const *s);
int						ft_tolower(int c);
char					*ft_itoa_base2(unsigned long long xxx, int base);
int						ft_len_base2(unsigned long long xxx, int base);
void					ft_extra_add(t_struct *ps, int reserve);
int						ft_reserve_full(t_struct *ps, int len);
void					ft_p(t_struct *ps, va_list ap);
int						ft_len_base(long long xxx, int base);
void					ft_d(t_struct *ps, va_list ap);
char					*ft_magic(char *fresh);
void					ft_d2(t_struct *ps, va_list ap);
void					ft_width_bigger_precision_p(t_struct *ps,
													char *fresh, int len);
void					ft_width_less_precision_p(t_struct *ps,
													char *fresh, int len);
void					ft_n(va_list ap);
#endif
