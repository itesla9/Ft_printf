/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 13:06:24 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/01 12:27:55 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void				ft_putnbr2(unsigned long long n, t_struct *ps)
{
	if (ps->precision == 0 && n == 0)
		return ;
	if (n >= 10)
	{
		ft_putnbr2(n / 10, ps);
		ft_putnbr2(n % 10, ps);
	}
	else
		ft_putchar(n + '0');
}
