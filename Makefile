#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/05/13 15:10:35 by yteslenk          #+#    #+#              #
#    Updated: 2017/05/13 15:10:36 by yteslenk         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = libftprintf.a

CFLAGS = -Wall -Wextra -Werror
SRC =	adjust.c \
		base.c \
		c.c \
		d.c \
		d2.c \
		f.c \
		ft_putnbr2.c \
		o.c \
		o2.c \
		p.c \
		ft_printf.c \
		s.c \
		und.c \
		x.c \
		x2.c \
		xxx.c \
		ft_putchar.c \
		ft_putstr.c \
		ft_tolower.c \
		ft_atoi.c \
		ft_strlen2.c
 
OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME):
	gcc $(CFLAGS) -c $(SRC)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all

trash: 
	rm *~