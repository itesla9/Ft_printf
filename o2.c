/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   o2.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/30 17:58:58 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/01 12:22:00 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	ft_hyphen1_0_o(t_struct *ps, char *fresh, int reserve, int reserve_int)
{
	if (ps->hyphen == 1)
	{
		if (ps->shark == 1)
			ft_putchar('0');
		while (reserve_int > 0)
		{
			ft_putchar('0');
			reserve_int--;
		}
		ft_putstr(fresh);
		ft_putspace(reserve, ps);
	}
	if (ps->hyphen == 0)
	{
		ft_putspace(reserve, ps);
		if (ps->shark == 1)
			ft_putchar('0');
		while (reserve_int > 0)
		{
			ft_putchar('0');
			reserve_int--;
		}
		ft_putstr(fresh);
	}
}

void	ft_hyphen2_0_o(t_struct *ps, char *fresh, int reserve)
{
	if (ps->hyphen == 1)
	{
		if (ps->shark == 1)
			ft_putchar('0');
		ft_putstr(fresh);
		ft_putspace(reserve, ps);
	}
	if (ps->hyphen == 0)
	{
		ft_putspace(reserve, ps);
		if (ps->shark == 1)
			ft_putchar('0');
		if (fresh[0] != '0')
			ft_putstr(fresh);
	}
}

void	ft_hyphen3_02_o(t_struct *ps, char *fresh, int reserve)
{
	if (ps->hyphen == 0)
		ft_putspace(reserve, ps);
	if (ps->shark == 1)
		ft_putchar('0');
	ft_putstr(fresh);
}

void	ft_hyphen3_0_o(t_struct *ps, char *fresh, int reserve)
{
	if (fresh[0] == '0')
	{
		ft_putchar('0');
		return ;
	}
	if (ps->zero == 0)
	{
		if (ps->hyphen == 0)
		{
			ft_putspace(reserve, ps);
			if (ps->shark == 1)
				ft_putchar('0');
			ft_putstr(fresh);
		}
		if (ps->hyphen != 0)
		{
			if (ps->shark == 1)
				ft_putchar('0');
			ft_putstr(fresh);
			ft_putspace(reserve, ps);
		}
	}
	if (ps->zero == 1)
		ft_hyphen3_02_o(ps, fresh, reserve);
}
