/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/28 15:07:55 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/05 16:23:01 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	ft_n(va_list ap)
{
	int	*p;

	p = va_arg(ap, int*);
	*p = g_stars;
}

int		ft_reserve_full(t_struct *ps, int len)
{
	int	reserve;

	if (ps->precision > len)
		reserve = ps->width - ps->precision;
	else
		reserve = ps->width - len;
	return (reserve);
}

void	ft_extra_add(t_struct *ps, int reserve)
{
	while (reserve > 0)
	{
		if (ps->hyphen == 0 && ps->zero == 1)
			ft_putchar('0');
		else
			ft_putchar(' ');
		reserve--;
	}
}

void	additional_c(char letter, t_struct *ps, int reserve)
{
	if (ps->hyphen == 1)
	{
		ft_putchar(letter);
		ft_extra_add(ps, reserve);
	}
	if (ps->hyphen == 0)
	{
		ft_extra_add(ps, reserve);
		ft_putchar(letter);
	}
}

void	ft_c(t_struct *ps, va_list ap)
{
	char	letter;
	int		reserve;

	reserve = ps->width - 1;
	letter = va_arg(ap, int);
	if (ps->width == 0 || ps->width == 1)
		ft_putchar(letter);
	else
		additional_c(letter, ps, reserve);
}
